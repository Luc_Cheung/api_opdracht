<?php

namespace App\Http\Controllers;

// use Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\posts;
use Validator;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function add_post(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'user_id' => 'required',
            'body' => 'required',
        ]);

        if ($validated->fails()) {
            return response()->json($validated->errors(), 400);
        }

        $post = Posts::create([
            'user_id' => $request->id,
            'body' => $request->body,
        ]);

        return response()->json(['message' => 'Post send', 'Post' => $post], 200);
    }

    public function get_posts()
    {
        $posts = Posts::get();
        return response()->json($posts, 200);
    }

    public function single_post(Request $request)
    {
        $post = Posts::find($request->id);
        return response()->json($post, 200);
    }
}
