<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use Carbon\Carbon;
use App\Models\User;

class AuthController extends Controller
{
    public function logout(Request $request)
    {
        auth()->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }
    
    public function login(Request $request) {
        $validated = validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validated->fails()) {
            return response()->json($validated->errors(), 401);
        }

        $token = auth() -> attempt($validated->validated());
        return response()->json(['token' => $token], 200);

        // return response()->json(['data' => [
        //     'user' => Auth::user(),
        //     'access_token' => $tokenResult->accessToken,
        //     'token_type' => 'Bearer',
        //     'expires_at' => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString()
        // ]]);
    }


    public function register(Request $request) 
    {
        $validated = validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|unique:users|max:255',
            'password' => 'required|confirmed'
        ]);

        if($validated->fails()) {
            return response()->json($validated->errors(), 400);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        // $user->save();

        return response()->json(['message' => 'Gebruiker registeren gelukt', 'user' => $user], 200);
    }
        
}
